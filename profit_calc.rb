#pajak dikenakan jika pendapatan lebih besar daripada pengeluaran
#pajak senilai n% dari laba kotor (pendapatan - pengeluaran)
def tax_calc(revenue, cost, tax_percent)

end

#besarnya net income senilai pendapatan - pengeluaran - pajak
def net_income_calc(revenue, cost, tax_percent)

end

#besarnya margin senilai rasio net income dibandingkan pendapatan
#desimal margin dibulatkan dalam 2 digit
#margin dinyatakan dalam persen
def margin_calc(revenue, cost, tax_percent)

end

puts "Input total pendapatan: "
revenue = gets.chomp.to_f
puts "Input total biaya: "
cost = gets.chomp.to_f
puts "Input pajak (dalam persen): "
tax_percent = gets.chomp.to_f

puts "Total pendapatan: #{revenue}"
puts "Total pengeluaran: #{cost}"
puts "Total pajak: #{tax_calc(revenue, cost, tax_percent)}"
puts "Keuntungan: #{net_income_calc(revenue, cost, tax_percent)}"
puts "Margin(dalam persen): #{margin_calc(revenue, cost, tax_percent)} %"